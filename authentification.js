var token;
var storageJSON;
var storage_token;

function authentification() {
    let email = document.getElementById("inputEmail").value;
    let pwd = document.getElementById("inputPassword").value;
    console.log(email);
    console.log(pwd);

    fetch("http://127.0.0.1:8055/auth/login/", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
            {
                'email': `${email}`,
                'password': `${pwd}`
            }
        )
    })
        .then((response) => response.json())
        .then((data) => {
            token = localStorage.setItem('access_token', JSON.stringify(data));
            storageJSON = localStorage.getItem('access_token');
            token = JSON.parse(storageJSON).data.access_token;
            storage_token = localStorage.setItem('access_token', token);
            token = localStorage.getItem('access_token');

            console.log(data);
            location.replace('accueil.html');
        })
        .catch((error) => {
            console.error('Error:', error);
            window.alert("Identifiant ou mot de passe incorrect !");
        });
}

function deconnexion () {
    token = localStorage.clear();
    location.replace("authentification.html")
    console.log(token);
}
/* Fonction GET pour les blocs */
document.onload=bloc()

function bloc() {
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer Ny9TfMXiOD5HMC8qiAQKitMsJY89L5LK");

    var requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow'
    }
    fetch("http://127.0.0.1:8055/items/bloc/", requestOptions)
    
    .then(response => response.text())
    .then(result => {
        let resultJSON = console.log(JSON.parse(result));

        let bloc_1 = result.substring(64, 119);
        console.log(bloc_1);

        let bloc_2 = result.substring(177, 219);
        console.log(bloc_2);

        let bloc_3 = result.substring(277, 317);
        console.log(bloc_3);

        let bloc_competences = bloc_1 + bloc_2 + bloc_3;
        console.log(bloc_competences);

        document.getElementById('list-bloc-select').innerHTML+='<option>'+[bloc_competences]+'</option>';
        
    })
    .catch(error => console.log('error', error));
}

/* Fonction GET pour les compétences */ 

function get_competences () {
    var myHeaders = new Headers();
myHeaders.append("Authorization", "Bearer Ny9TfMXiOD5HMC8qiAQKitMsJY89L5LK");

var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

fetch("http://127.0.0.1:8055/items/competences", requestOptions)
  .then(response => response.text())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));
}
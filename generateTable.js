function ajouter() {
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer Ny9TfMXiOD5HMC8qiAQKitMsJY89L5LK");
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
        "nom_eval": `${document.getElementById("titre_eval").value}`
    });

    var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
    };

    fetch("http://127.0.0.1:8055/items/evaluations", requestOptions)
    .then(response => response.text())
    .then(result => console.log(result))
    .catch(error => console.log('error', error));
}

function critere() {

    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer Ny9TfMXiOD5HMC8qiAQKitMsJY89L5LK");

    var raw = JSON.stringify({
        "nom_critere": `${document.getElementById("nom_critere").value}`
    });
    
    var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
    };

    fetch("http://127.0.0.1:8055/items/criteres", requestOptions)
    .then(response => response.text())
    .then(result => console.log(result))
    .catch(error => console.log('error', error));
}

var l = 0;

function addTable() {
    var Table = document.getElementById("create-table");
    var ligne = document.createElement("tr");
    var td1 = document.createElement("create-td");
    var td2 = document.createElement("create-td");

    var textTD1 = document.createElement("input");
    textTD1.setAttribute("type", "text");
    textTD1.setAttribute("class", "create-td");
    textTD1.setAttribute("class", "create-text-eval");
    textTD1.setAttribute("id", "titre-eval" + (l+1));

    var textTD2 = document.createElement("input");
    textTD2.setAttribute("type", "text");
    textTD2.setAttribute("class", "create-td");
    textTD2.setAttribute("class", "create-text-eval");
    textTD2.setAttribute("id", "critere-eval" + (l+1));

    Table.appendChild(ligne);
    ligne.appendChild(td1);
    ligne.appendChild(td2);
    td1.appendChild(textTD1);
    td2.appendChild(textTD2);

    l=l+1;
}

function delTable() {
    document.getElementById("create-table").deleteRow(-1);
}

function supprimer(){
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer Ny9TfMXiOD5HMC8qiAQKitMsJY89L5LK");

    var raw = JSON.stringify({
        "id_eval": `${document.getElementById("titre-eval")}, ${document.getElementById("nom_critere")}`
    })

    var requestOptions = {
    method: 'DELETE',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
    };

    fetch("http://127.0.0.1:8055/items/evaluations/", requestOptions)
    .then(response => response.text())
    .then(result => console.log(result))
    .catch(error => console.log('error', error));
}